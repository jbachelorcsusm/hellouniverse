# Classroom Code, 2018.01.22: HelloUniverse #

The very beginning! This is just a default blank Xamarin.Forms app as it comes out of the box, with a few debug lines added in to start to learn more about the app lifecycle.

### Questions? ###

* Always always always: If you have a question, someone else in class almost certainly has that same question, so please do ask!
* [Post questions on Cougar Courses](https://cc.csusm.edu/mod/oublog/view.php?id=630348)